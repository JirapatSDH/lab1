using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Soisuwan.GameDev3.Chapter8
{
    public class MyAnimatorControlScript : MonoBehaviour
    {
        public Slider slider;
        public Toggle tog;
        protected Animator m_Animator;
        private static readonly int Punch = Animator.StringToHash("Punch");
        private static readonly int Dancing = Animator.StringToHash("Dancing");
        private static readonly int State = Animator.StringToHash("State");
        private static readonly int Turn = Animator.StringToHash("Turn");
        private static readonly int Forward = Animator.StringToHash("Forward");
        // Start is called before the first frame update
        void Start()
        {
        m_Animator = GetComponent<Animator>();
        }
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space)){
                m_Animator.SetTrigger("Jump");
            }
            if (Input.GetKeyDown(KeyCode.Z)){
                m_Animator.SetTrigger(Punch);
            }
            if (Input.GetKeyDown(KeyCode.X)){
                m_Animator.SetBool(Dancing,true);
            }
            if (Input.GetKeyDown(KeyCode.C)){
                m_Animator.SetInteger(State,2);
            }

            if (Input.GetKeyDown(KeyCode.V))
            {
                m_Animator.SetFloat(Turn,0.3f);
            }
        }
        public void ChangeSliderValue()
        {
            float speedValue = slider.value;
            m_Animator.SetFloat(Forward, speedValue);
        }

        public void SetDance()
        {
            if (tog.isOn)
            {
                m_Animator.SetBool(Dancing,true);
            }
        }
    }
}