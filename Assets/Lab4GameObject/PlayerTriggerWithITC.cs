using System;
using System.Collections;
using System.Collections.Generic; 
using System.Diagnostics;
using Soisuwan.GameDev3.Chapter1;
using Soisuwan.GameDev3.Chapter3;
using UnityEngine;

namespace Soisuwan.GameDev3.Chapter4
{
    public class PlayerTriggerWithITC : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            ItemTypeComponent itc = other.GetComponent<ItemTypeComponent>();
            var inventory = GetComponent<Inventory>();
            var simpleHP = GetComponent<SimpleHealthComponent>();
            var jump = GetComponent<Rigidbody>();
            if (itc != null)
            {
                switch (itc.Type)
                {
                    case ItemType.COIN:
                        inventory.AddItem("COIN",1);
                        break;
                    case ItemType.BIGCOIN:
                        inventory.AddItem("BIGCOIN",1);
                        break;
                    case ItemType.POWERUP:
                        if(simpleHP != null)
                            simpleHP.HealthPoint = simpleHP.HealthPoint + 10;
                        break;
                    case ItemType.POWERDOWN:
                        if(simpleHP != null)
                            simpleHP.HealthPoint = simpleHP.HealthPoint - 10; 
                        break;
                    case ItemType.JUMP:
                        if (jump != null)
                            transform.Translate(transform.forward * 5 * 10, Space.World);
                        break;
                    case ItemType.DIE:
                        Destroy(gameObject);
                        break;
                }
            }
            Destroy(other.gameObject,0);
        }
    }
}