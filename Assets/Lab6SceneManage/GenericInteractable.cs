using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Soisuwan.GameDev3.Chapter5.InteractionSystem;

namespace Soisuwan.GameDev3.Chapter6.UnityEvents
{
    public class GenericInteractable : MonoBehaviour, IInteractable,IActorEnterExitHandler
    {
        [SerializeField] protected UnityEvent m_OnInteract = new();
        [SerializeField] protected UnityEvent m_OnActorEnter = new();
        [SerializeField] protected UnityEvent m_OnActorExit = new();// Start is called before the first frame update

        [SerializeField] protected UnityEvent<GameObject> m_OnInteractGameObj = new();
        [SerializeField] protected UnityEvent<GameObject> m_OnActorEnterGameObj = new();
        [SerializeField] protected UnityEvent<GameObject> m_OnActorExitGameObj = new();
        public virtual void Interact(GameObject actor)
        {
            m_OnInteract.Invoke();
            m_OnInteractGameObj.Invoke(actor);
        }

        public virtual void ActorEnter(GameObject actor)
        {
            
            m_OnActorEnter.Invoke();
            m_OnActorEnterGameObj.Invoke(actor);
        }

        public virtual void ActorExit(GameObject actor)
        {
            m_OnActorExit.Invoke();
            m_OnActorExitGameObj.Invoke(actor);
        }
    }
}