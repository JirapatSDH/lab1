using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;

namespace Soisuwan.GameDev3.Chapter6.UnityEvents
{
    public class Timer : MonoBehaviour
    {
        [SerializeField] protected float m_TimerDuration = 5;
        [SerializeField] protected UnityEvent m_TimerStartEvent = new();
        [SerializeField] protected UnityEvent m_TimerEndEvent = new();
        [SerializeField] protected TextMeshProUGUI m_TextInfoEToInteract;
        [SerializeField] protected Slider m_SliderTimer;
        protected bool _IsTimerStart = false;
        protected float _StartTimeStamp;
        protected float _EndTimeStamp;
        private float _SliderValue;
        public GameObject _GameObject;
        [SerializeField] protected float _CurrentTime;

        private void Update()
        {
            if (!_IsTimerStart) return;
            _CurrentTime = (Time.time - _StartTimeStamp);
            _SliderValue = ((Time.time - _StartTimeStamp)/m_TimerDuration)* m_SliderTimer.maxValue;
            m_SliderTimer.value = _SliderValue;
            if (Time.time >= _EndTimeStamp)
            {
                EndTimer();
            }
        }

        public virtual void StartTimer()
        {
            if (_IsTimerStart) return;
            m_TimerStartEvent.Invoke();
            _IsTimerStart = true;
            _StartTimeStamp = Time.time;
            _EndTimeStamp = Time.time + m_TimerDuration;
            

        }
        public virtual void EndTimer()
        {
            m_TimerEndEvent.Invoke(); 
            _IsTimerStart = false;
            _GameObject.SetActive(false);
        }
    }
}