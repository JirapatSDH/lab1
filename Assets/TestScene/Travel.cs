using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class Travel : MonoBehaviour
{
    [SerializeField] private GameObject presents, past;

    [SerializeField] private bool presentVisit = true;

    private float timeEffect = 0f;

    [SerializeField] private float EffectTimeRatePerSec = 2f;

    [SerializeField] private Volume effectVolum;

    [SerializeField] private float transition = 2f;
    // Start is called before the first frame update
    void Start()
    {
        presents.SetActive(presentVisit);
            past.SetActive(!presentVisit);
            effectVolum.weight = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire2") && Time.time >= timeEffect)
        {
            timeEffect = Time.time + 1 / EffectTimeRatePerSec;
            presents.SetActive(!presents.activeSelf);
            past.SetActive(!past.activeSelf);
            effectVolum.weight = 1;
            Invoke("TurnOfEffect",transition);
        }
    }

    private void TurnOfEffect()
    {
        effectVolum.weight = 0;
    }
}
