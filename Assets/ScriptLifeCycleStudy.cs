using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;
using UnityEngine;

namespace Soisuwan.GameDev3.Chapter1
{
    public class ScriptLifeCycleStudy : MonoBehaviour
    {
        private int m_callingSequence = 1;

        void Awake()
        {
            Debug.LogFormat("{0}{1}has been called.",m_callingSequence++,
                MethodBase.GetCurrentMethod().Name);
        }

        // Start is called before the first frame update
        void Start()
        {
            Debug.LogFormat("{0}{1}has been called.",m_callingSequence++,
                MethodBase.GetCurrentMethod().Name);
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        private void FixedUpdate()
        {
            
        }

         void OnEnable()
        {
            Debug.LogFormat("{0}{1} has been called.",m_callingSequence++,
                MethodBase.GetCurrentMethod().Name);
        }

         void OnDisable()
        {
            Debug.LogFormat("{0}{1} has been called.",m_callingSequence++,
            MethodBase.GetCurrentMethod().Name);
        }

         void OnDestroy()
        {
            Debug.LogFormat("{0}{1} has been called.",m_callingSequence++,
                MethodBase.GetCurrentMethod().Name);
        }

         void OnApplicationPause(bool pauseStatus)
        {
            Debug.LogFormat("{0}{1} has been called.",m_callingSequence++,
                MethodBase.GetCurrentMethod().Name);
        }

         void OnApplicationQuit()
        {
            Debug.LogFormat("{0}{1} has been called.",m_callingSequence++,
                MethodBase.GetCurrentMethod().Name);
        }
    }
}

