using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Soisuwan.GameDev3.Chapter5.InteractionSystem;

namespace Soisuwan.GameDev3.Chapter9
{
    public class ActorTriggerHandlerV2 : ActorTriggerHandler
    {
        public virtual IInteractable[] GetInteractables()
        {
            //Remove null object from the list
            m_TriggeredGameObjects.RemoveAll(gameobject => gameobject == null);
           
            if (m_TriggeredGameObjects.Count == 0){
                return null;
            }

            List<IInteractable> interactableList = new();
            foreach (var g in m_TriggeredGameObjects)
            {
                interactableList.Add(g.GetComponent<IInteractable>());
            }

            return interactableList.ToArray();
        }
    }
}